import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { MistApi } from './MistApi';

/**
 * Class to interact with `Aseq` route of MiST API
 *
 * @export
 * @class Aseq
 */
export class Aseq {
  private entity: string;
  private readonly localBaseUrl: string;
  private readonly logger: Logger;
  private readonly logLevel: LogLevelDescType;

  /**
   * Creates an instance of Aseq.
   * @param {LogLevelDescType} [logLevel='INFO']
   * @param {string} [localBaseUrl='']
   * @memberof Aseq
   */
  constructor(logLevel: LogLevelDescType = 'INFO', localBaseUrl: string = '') {
    this.entity = 'aseq';
    this.logLevel = logLevel;
    this.logger = new Logger(this.logLevel);
    this.localBaseUrl = localBaseUrl;
  }

  /**
   * Fetch info related to 1 aseq id
   *
   * @param {string} aseqId
   * @returns
   * @memberof Aseq
   */
  public async fetch(aseqId: string) {
    const log = this.logger.getLogger('Aseq::fetch');
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const genesUrl = `${mistApi.getBaseUrl(this.entity)}/${aseqId}`;
    return mistApi.fetch(genesUrl).then(data => {
      // tslint:disable-next-line: no-unsafe-any
      log.info(`The call for fetching all info about ${aseqId} has been answered with a record`);

      return data;
    });
  }

  /**
   * Fetch info of many aseq ids
   *
   * @param {string[]} aseqIds
   * @returns
   * @memberof Aseq
   */
  public async fetchMany(aseqIds: string[]) {
    const log = this.logger.getLogger('Aseq::fetchMany');
    const rest = aseqIds.splice(1000);
    const moreData: any[] = rest.length === 0 ? [] : await this.fetchMany(rest);
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const genesUrl = `${mistApi.getBaseUrl(this.entity)}`;
    return mistApi
      .fetchPost(genesUrl, aseqIds)
      .then(data => {
        // tslint:disable-next-line: no-unsafe-any
        log.info(`The call for fetching aseqIds of ${aseqIds.length} aseqs has been answered.`);
        if (moreData.length) {
          return moreData.concat(data);
        }
        return data;
      })
      .catch(err => {
        log.error(err.response.data);
        if (err.response.data.code === 400 && err.response.data.message.match(/^Invalid item in position/)) {
          log.error('Please clean your list of aseqs');
        }
        throw err;
      });
  }
}
