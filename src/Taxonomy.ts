import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { MistApi } from './MistApi';

/**
 * Class to interact with `taxonomy` route of MiST API
 *
 * @export
 * @class Taxonomy
 */
export class Taxonomy {
  private entity: string;
  private readonly localBaseUrl: string;
  private readonly logger: Logger;
  private readonly logLevel: LogLevelDescType;

  /**
   * Creates an instance of Taxonomy.
   * @param {LogLevelDescType} [logLevel='INFO']
   * @param {string} [localBaseUrl='']
   * @memberof Taxonomy
   */
  constructor(logLevel: LogLevelDescType = 'INFO', localBaseUrl: string = '') {
    this.entity = 'taxonomy';
    this.logLevel = logLevel;
    this.logger = new Logger(this.logLevel);
    this.localBaseUrl = localBaseUrl;
  }

  /**
   * It returns the parents from a given taxonomy id.
   *
   * @param {number} taxid
   * @param {boolean} [options={skipFailed: false}]
   * @returns
   * @memberof Taxonomy
   */
  public getParents(taxid: number) {
    const log = this.logger.getLogger('Taxonomy::getParents');
    log.info(`Getting parents of taxid: ${taxid}`);
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const url = `${mistApi.getBaseUrl(this.entity)}/${taxid}/parents?`;
    return mistApi.fetch(url).then(data => {
      log.info(`The call for fetching taxonomy parents of ${taxid} has been answered.`);
      log.info(data);
      return data;
    });
  }
}
