import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { IGenesMist3Api } from './interfaces';
import { MistApi } from './MistApi';

const MIST_GENE_NEIGHBORHOOD_LIMIT = 15;

export class Genes {
  private readonly entity: string;
  private readonly localBaseUrl: string;
  private readonly logger: Logger;
  private readonly logLevel: LogLevelDescType;

  /**
   * Creates an instance of Genes.
   * @param {LogLevelDescType} [logLevel='INFO']
   * @param {string} [localBaseUrl='']
   * @memberof Genes
   */
  public constructor(logLevel: LogLevelDescType = 'INFO', localBaseUrl: string = '') {
    this.entity = 'genes';
    this.logLevel = logLevel;
    this.localBaseUrl = localBaseUrl;
    this.logger = new Logger(this.logLevel);
  }

  /**
   * Fetches gene info by one gene by stable_id.
   *
   * @param {string} id
   * @returns
   * @memberof Genes
   */
  public async fetchByStableId(stableId: string) {
    const log = this.logger.getLogger('Genes::fetchByStableId');
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const url = `${mistApi.getBaseUrl(this.entity)}/${stableId}`;
    return mistApi.fetch(url).then(data => {
      // tslint:disable-next-line: no-unsafe-any
      log.info(`The call for fetching gene ${stableId} has been answered`);
      return data;
    });
  }

  /**
   * Fetches gene info by one or more gene ids separated by comma.
   *
   * @param {string} id
   * @returns
   * @memberof Genes
   */
  public async fetchByIds(id: string) {
    if (id === '') {
      return [];
    }
    const log = this.logger.getLogger('Genes::fetchById');
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const url = `${mistApi.getBaseUrl(this.entity)}?where.id=${id}`;
    return mistApi.fetch(url, true, true).then(data => {
      // tslint:disable-next-line: no-unsafe-any
      log.info(
        `The call for fetching ${id.split(',').length} gene ids has been answered with ${data.length} responses`,
      );
      return data;
    });
  }

  /**
   * (Experimental) Fetches gene info by one or more stable_ids separated by comma.
   *
   * @param {string} stableIds
   * @returns
   * @memberof Genes
   */
  public async fetchByStableIds(stableIds: string) {
    if (this.localBaseUrl === '') {
      throw new Error('This function is experimental, only works to next version of MiST3.');
    }
    if (stableIds === '') {
      return [];
    }
    const log = this.logger.getLogger('Genes::fetchByStableIds');
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const url = `${mistApi.getBaseUrl(this.entity)}?where.stable_id=${stableIds}`;
    return mistApi.fetch(url, true, true).then(data => {
      // tslint:disable-next-line: no-unsafe-any
      log.info(
        `The call for fetching ${stableIds.split(',').length} gene ids has been answered with ${data.length} responses`,
      );
      return data;
    });
  }

  /**
   * Fetches gene info by any field.
   *
   * @param {string} anything
   * @returns
   * @memberof Genes
   */
  public async fetchByAnyField(anything: string) {
    const log = this.logger.getLogger('Genes::fetchByAnyField');
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const url = `${mistApi.getBaseUrl(this.entity)}?search=${anything}&`;
    return mistApi.fetch(url, true, true).then(data => {
      // tslint:disable-next-line: no-unsafe-any
      log.info(`The call for fetching gene based on "${anything}" has been answered with ${data.length} results.`);
      data.forEach((element: any) => {
        log.info(`stableId = ${element.stable_id}`);
      });
      return data;
    });
  }

  /**
   * Fetch genes neighbohring a given gene (mist3 stable_id)
   *
   * @param {string} stableId
   * @param {number} downstream
   * @param {number} upstream
   * @returns
   * @memberof Genes
   */
  public async getGeneHood(stableId: string, downstream: number, upstream: number) {
    const log = this.logger.getLogger('Genes::getGeneHood');
    const mainGene = await this.fetchByStableId(stableId);
    const downRounds = this.calcRounds(downstream);
    const upRounds = this.calcRounds(upstream);

    const upInThisRound = upRounds.shift();
    const downInThisRound = downRounds.shift();

    let data = await this.getGeneHoodLimited(mainGene, downInThisRound, upInThisRound);
    data.push(mainGene);
    data.sort((a: IGenesMist3Api, b: IGenesMist3Api) => {
      if (a.start && b.start) {
        return a.start - b.start;
      }
      return 0;
    });
    log.debug(`data: ${data.map((d: IGenesMist3Api) => d.start)}`);

    while (upRounds.length > 0) {
      const gene = data[0];
      const moreData = await this.getGeneHoodLimited(gene, 0, upRounds.shift());
      if (moreData.length === 0) {
        break;
      }
      data = moreData.concat(data);
    }

    while (downRounds.length > 0) {
      const gene = data[data.length - 1];
      const moreData = await this.getGeneHoodLimited(gene, downRounds.shift(), 0);
      if (moreData.length === 0) {
        break;
      }
      data = data.concat(moreData);
    }

    return data;
  }

  /**
   * Fetch genes neighbohring a given gene (mist3 stable_id) limited to 15 down and upstream
   *
   * @param {string} stableId
   * @param {number} downstream
   * @param {number} upstream
   * @returns
   * @memberof Genes
   */
  private async getGeneHoodLimited(mainGene: IGenesMist3Api, downstream: number, upstream: number) {
    const log = this.logger.getLogger('Genes::getGeneHoodLimited');
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const stableId = mainGene.stable_id;
    const url = `${mistApi.getBaseUrl(
      this.entity,
    )}/${stableId}/neighbors?amountBefore=${upstream}&amountAfter=${downstream}`;

    return mistApi.fetch(url).then(data => {
      // tslint:disable-next-line: no-unsafe-any
      log.info(`The call for fetching gene neighbohood of ${stableId} has been answered.`);
      return data;
    });
  }

  private calcRounds(n: number) {
    const numOfrounds = Math.floor(n / MIST_GENE_NEIGHBORHOOD_LIMIT);
    const rest = n % MIST_GENE_NEIGHBORHOOD_LIMIT;
    const rounds = Array(numOfrounds).fill(MIST_GENE_NEIGHBORHOOD_LIMIT);
    rounds.push(rest);
    return rounds;
  }
}
