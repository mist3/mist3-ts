/**
 * Experimental MiST3 interface for genomes objects returned by the API
 *
 * @interface IGenomeMist3Api
 */
interface IGenomeMist3Api {
  id: 1486;
  worker_id: number | null;
  accession: string | null;
  version: string | null;
  version_number: number | null;
  genbank_accession: string | null;
  genbank_version: string | null;
  taxonomy_id: number | null;
  name: string | null;
  refseq_category: string | null;
  bioproject: string | null;
  biosample: string | null;
  wgs_master: string | null;
  isolate: string | null;
  version_status: string | null;
  assembly_level: string | null;
  release_type: string | null;
  release_date: string | null;
  assembly_name: string | null;
  submitter: string | null;
  ftp_path: string | null;
  superkingdom: string | null;
  phylum: string | null;
  class: string | null;
  order: string | null;
  family: string | null;
  genus: string | null;
  species: string | null;
  strain: string | null;
  stats: any | {};
  createdAt: string | null;
  updatedAt: string | null;
  WorkerId: number | null;
}

/**
 * Experimental MiST3 interface for pfam hit objects returned by the API
 *
 * @interface IPfamMist3API
 */
interface IPfamMist3API {
  name: string;
  score: number;
  bias: number;
  c_evalue: number;
  i_evalue: number;
  hmm_from: number;
  hmm_to: number;
  hmm_cov: string;
  ali_from: number;
  ali_to: number;
  ali_cov: string;
  env_from: number;
  env_to: number;
  env_cov: string;
  acc: number;
}

/**
 * Experimental MiST3 interface for aseq objects returned by the API
 *
 * @interface IAseqMist3Api
 */
interface IAseqMist3Api {
  pfam31: IPfamMist3API[];
  agfam2: any[];
  ecf1: any[];
  stp: {
    version: number | null;
  };
  id: string;
  length: number;
  sequence: string;
  segs: any[];
  coils: any[];
  tmhmm2: any;
}

/**
 * Experimental MiST3 interface for gene objects returned by the API
 *
 * @interface IGenesMist3Api
 */
interface IGenesMist3Api {
  id: number;
  stable_id: string | null;
  component_id: number | null;
  dseq_id: string | null;
  aseq_id: string | null;
  accession: string | null;
  version: string | null;
  locus: string | null;
  old_locus: string | null;
  location: string | null;
  strand: string | null;
  start: number | null;
  stop: number | null;
  length: number | null;
  names: string[] | null;
  pseudo: boolean;
  notes: string | null;
  product: string | null;
  codon_start: number | null;
  translation_table: number | null;
  qualifiers: any;
  cds_location: string | null;
  cds_qualifiers: {
    inference: string | null;
  };
  ComponentId: number | null;
  AseqId: string | null;
  DseqId: string | null;
}

interface ISignalFilter {
  kind: string;
  function?: string;
  ranks?: string;
}

/**
 * Indexed fields for searching /genomes
 *
 * @interface IGenomeQuery
 */
interface IGenomeQuery {
  id?: string;
  taxonomy_id?: string;
  name?: string;
  superkingdom?: string;
  phylum?: string;
  class?: string;
  order?: string;
  family?: string;
  genus?: string;
  assembly_level?: string;
}

export { IGenomeQuery, ISignalFilter, IAseqMist3Api, IGenesMist3Api, IGenomeMist3Api, IPfamMist3API };
