import Axios from 'axios';
import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Entities } from './entities';

/**
 * Base Url Building for MiST3 API endpoints
 *
 * @export
 * @MistApi
 */
export class MistApi {
  private BASE_URL: string = 'https://api.mistdb.caltech.edu/v1';
  private readonly GENES_ROOT: string = '/genes';
  private readonly GENOMES_ROOT: string = '/genomes';
  private readonly TAXONOMY: string = '/taxonomy';
  private readonly ASEQ: string = '/aseqs';

  private readonly ENTITY_TO_BASEURL: Map<string, string>;

  private readonly log: Logger;

  /**
   * Creates an instance of MistApi.
   * @param {LogLevelDescType} [logLevel='INFO']
   * @param {string} [localBaseUrl='']
   * @memberof MistApi
   */
  constructor(logLevel: LogLevelDescType = 'INFO', localBaseUrl: string = '') {
    const logger = new Logger(logLevel);
    this.log = logger;
    this.BASE_URL = localBaseUrl ? localBaseUrl : this.BASE_URL;
    this.ENTITY_TO_BASEURL = new Map([
      [Entities.GENOMES, this.BASE_URL + this.GENOMES_ROOT],
      [Entities.GENOMES_SHOPCART, this.BASE_URL + this.GENOMES_ROOT],
      [Entities.SCOPE, this.BASE_URL + this.GENOMES_ROOT],
      [Entities.GENOME, this.BASE_URL + this.GENOMES_ROOT],
      [Entities.GENES, this.BASE_URL + this.GENES_ROOT],
      [Entities.GENES_SHOPCART, this.BASE_URL + this.GENES_ROOT],
      [Entities.GENE, this.BASE_URL + this.GENES_ROOT],
      [Entities.NEIGHBOUR_GENES, this.BASE_URL + this.GENES_ROOT],
      [Entities.SIGNAL_GENES, this.BASE_URL + this.GENOMES_ROOT],
      [Entities.TAXONOMY, this.BASE_URL + this.TAXONOMY],
      [Entities.ASEQ, this.BASE_URL + this.ASEQ],
    ]);
  }

  public getBaseUrl(entity: string): string {
    const url = this.ENTITY_TO_BASEURL.get(entity);
    const log = this.log.getLogger('MistApi::getBaseUrl');
    log.debug(`url: ${url}`);
    if (typeof url === 'undefined') {
      throw Error(`Unknown entity: ${entity}`);
    }
    return url;
  }

  public async fetch(
    url: string,
    next: boolean = false,
    withPage: boolean = false,
    page: number = 1,
    perPage: number = 100,
  ) {
    const log = this.log.getLogger('MistApi::fetch');
    const pagedUrl = `${url}&page=${page}&per_page=${perPage}`;
    const finalUrl = withPage ? pagedUrl : url;
    log.info(`url: ${finalUrl}`);

    return Axios.get(finalUrl)
      .then(async (res: { status: any; data: any }) => {
        log.info(`${finalUrl} has status: ${res.status}`);
        let data = res.data;
        if (Array.isArray(data)) {
          log.info(`Fetched ${data.length}`);
          if (next && data.length) {
            const newData = await this.fetch(url, next, withPage, page + 1, perPage).catch((err: Error) => {
              throw err;
            });
            data = data.concat(newData);
            // tslint:disable-next-line: no-unsafe-any
            log.info(`Total: ${data.length}`);
            return data;
          } else {
            return data;
          }
        } else {
          return data;
        }
      })
      .catch(err => {
        log.error(err);
        throw err;
      });
  }

  public async fetchPost(url: string, load: any, next: boolean = false, page: number = 1, perPage: number = 100) {
    const log = this.log.getLogger('MistApi::fetchPost');
    log.info(`url: ${url}`);
    const options = load;
    options.page = page;
    options.per_page = perPage;
    log.debug(options);
    return Axios.post(url, options)
      .then(async (res: { status: any; data: any }) => {
        log.info(`POST call to ${url} has status: ${res.status}`);
        let data = res.data;
        if (Array.isArray(data)) {
          if (next && data.length) {
            const newData = await this.fetchPost(url, load, next, page + 1, perPage).catch((err: Error) => {
              throw err;
            });
            data = data.concat(newData);
            // tslint:disable-next-line: no-unsafe-any
            return data;
          } else {
            return data;
          }
        } else {
          return data;
        }
      })
      .catch(err => {
        log.error(err.response.data);
        throw err;
      });
  }
}
