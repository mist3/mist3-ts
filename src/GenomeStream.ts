import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Readable } from 'stream';
import { IGenomeQuery, ISignalFilter } from './interfaces';
import { MistApi } from './MistApi';

/**
 * Stream results from route /genomes in MiST3 API
 *
 * @export
 * @class GenomeStream
 */
export class GenomeStream {
  private readonly perPage: number;
  private readonly localBaseUrl: string;
  private readonly entity: string;
  private readonly logger: Logger;
  private readonly logLevel: LogLevelDescType;

  /**
   * Creates an instance of GenomeStream.
   * @param {number} [perPage=100]
   * @param {LogLevelDescType} [logLevel='INFO']
   * @param {string} [localBaseUrl='']
   * @memberof GenomeStream
   */
  public constructor(perPage: number = 100, logLevel: LogLevelDescType = 'INFO', localBaseUrl: string = '') {
    this.entity = 'genome';
    this.perPage = perPage;
    this.logLevel = logLevel;
    this.localBaseUrl = localBaseUrl;
    this.logger = new Logger(this.logLevel);
  }

  /**
   * Stream all genes of the genome starting from specific page
   *
   * @param {string} version
   * @param {number} [page=1]
   * @returns
   * @memberof GenomeStream
   */
  public async fetchAllGenes(version: string, page: number = 1) {
    const geneStream = new Readable({
      objectMode: true,
      read(size) {
        return true;
      },
    });
    const logging = this.logger.getLogger('Genome::fetchAllGenes');
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const genesUrl = `${mistApi.getBaseUrl(this.entity)}/${version}/genes?`;
    const fetchPages = async (p: number) => {
      await mistApi.fetch(genesUrl, false, true, p, this.perPage).then(data => {
        logging.debug(`received p ${p} with ${data.length} genes`);
        if (data.length) {
          p++;
          logging.info(`The call for fetching all genes of ${version} has been answered with ${data.length} genes`);
          data.forEach((item: any) => geneStream.push(item));
          return fetchPages(p);
        } else {
          logging.info(`No more data`);
          geneStream.push(null);
          return;
        }
      });
    };
    fetchPages(page);
    return geneStream;
  }
}
