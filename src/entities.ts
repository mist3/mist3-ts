export enum Entities {
  ASEQ = 'aseq',
  GENES = 'genes',
  GENES_SHOPCART = 'genes_shopcart',
  NEIGHBOUR_GENES = 'neighbour_genes',
  GENE = 'gene',
  GENOMES = 'genomes',
  GENOMES_SHOPCART = 'genomes_shopcart',
  GENOME = 'genome',
  SCOPE = 'scope',
  SIGNAL_GENES = 'signal_genes',
  TAXONOMY = 'taxonomy',
}
