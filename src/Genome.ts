import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { IGenomeQuery, ISignalFilter } from './interfaces';
import { MistApi } from './MistApi';

/**
 * Explore route /genomes in MiST3 API
 *
 * @export
 * @class Genome
 */
export class Genome {
  private readonly entity: string;
  private readonly localBaseUrl: string;
  private readonly logger: Logger;
  private readonly logLevel: LogLevelDescType;

  /**
   * Creates an instance of Genome.
   * @param {LogLevelDescType} [logLevel='INFO']
   * @param {string} [localBaseUrl='']
   * @memberof Genome
   */
  public constructor(logLevel: LogLevelDescType = 'INFO', localBaseUrl: string = '') {
    this.entity = 'genome';
    this.logLevel = logLevel;
    this.localBaseUrl = localBaseUrl;
    this.logger = new Logger(this.logLevel);
  }

  /**
   * Fetch all genes of the genome starting from page.
   *
   * @param {string} version
   * @param {number} [page=1]
   * @returns
   * @memberof Genome
   */
  public async fetchAllGenes(version: string, page: number = 1) {
    const logging = this.logger.getLogger('Genome::fetchAllGenes');
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const genesUrl = `${mistApi.getBaseUrl(this.entity)}/${version}/genes?`;
    return mistApi.fetch(genesUrl, true, true, page).then(data => {
      // tslint:disable-next-line: no-unsafe-any
      logging.info(`The call for fetching all genes of ${version} has been answered with ${data.length} genes`);

      return data;
    });
  }

  /**
   * Fetch all chemotaxis genes
   *
   * @param {string} version
   * @returns
   * @memberof Genome
   */
  public async fetchChemotaxis(version: string) {
    const logging = this.logger.getLogger('Genome::fetchChemotaxis');
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const chemotaxisUrl = `${mistApi.getBaseUrl(this.entity)}/${version}/signal-genes?where.ranks=chemotaxis&`;

    return mistApi.fetch(chemotaxisUrl, true, true).then(data => {
      // tslint:disable-next-line: no-unsafe-any
      logging.info(`The call for fetching chemotaxis genes of ${version} has been answered with ${data.length} genes`);
      return data;
    });
  }

  /**
   * Returns signal genes
   *
   * @param {string} [kind='']
   * @param {string} [func='']
   * @returns
   * @memberof Genome
   */
  public async fetchSignalGenes(version: string, filter: ISignalFilter = { kind: '' }) {
    const logging = this.logger.getLogger('Genome::fetchChemotaxis');

    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const options = this.filterToUrl(filter);
    const chemotaxisUrl = `${mistApi.getBaseUrl(this.entity)}/${version}/signal-genes?${options}&`;
    return mistApi.fetch(chemotaxisUrl, true, true).then(data => {
      // tslint:disable-next-line: no-unsafe-any
      logging.info(`The call for fetching chemotaxis genes of ${version} has been answered with ${data.length} genes`);
      return data;
    });
  }

  /**
   * Fetch details about the genome
   *
   * @param {string} version
   * @returns
   * @memberof Genome
   */
  public async fetchDetails(version: string) {
    const logging = this.logger.getLogger('Genome::fetchDetails');
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const url = `${mistApi.getBaseUrl(this.entity)}/${version}?`;
    logging.info(`Fetching details of ${version}`);

    return mistApi.fetch(url).then(data => {
      logging.info(`The call for fetching details of ${version} has been answered`);
      return data;
    });
  }

  /**
   * Fetch details of many genomes at once
   *
   * @param {string[]} versions
   * @returns
   * @memberof Genome
   */
  public async fetchDetailsMany(versions: string[]) {
    const logging = this.logger.getLogger('Genome::fetchDetailsMany');
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const url = `${mistApi.getBaseUrl(this.entity)}`;
    const load = {
      'where.version': versions,
    };
    logging.info(`Fetching details of ${versions.length} genomes`);
    return mistApi.fetchPost(url, load, true).then(data => {
      logging.info(`The call for fetching details of ${versions.length} genomes has been answered`);
      return data;
    });
  }

  /**
   * Find genomes matching a string in any field
   *
   * @param {string} searchString
   * @returns
   * @memberof Genome
   */
  public async search(searchString: string) {
    const logging = this.logger.getLogger('Genome::search');
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const url = `${mistApi.getBaseUrl(this.entity)}?search=${searchString}&`;
    logging.info(`Fetching genomes details related to ${searchString}`);

    return mistApi.fetch(url, true, true).then(data => {
      logging.info(`The call for fetching genome details related to ${searchString} has been answered`);
      return data;
    });
  }

  /**
   * Find genomes matching several indexed fields in the MiST3 database
   *
   * @returns
   * @memberof Genome
   */
  public async find(query: IGenomeQuery) {
    const logging = this.logger.getLogger('Genome::find');
    const mistApi = new MistApi(this.logLevel, this.localBaseUrl);
    const parsedQuery = this.parseGenomeQuery(query);
    const url = `${mistApi.getBaseUrl(this.entity)}?${parsedQuery}&`;
    logging.info(`Finding genomes details`);

    return mistApi.fetch(url, true, true).then(data => {
      logging.info(`The call for finding genome details has been answered`);
      return data;
    });
  }

  /**
   * Parse "where" type of query
   *
   * @private
   * @param {IGenomeQuery} query
   * @returns
   * @memberof Genome
   */
  private parseGenomeQuery(query: IGenomeQuery) {
    return Object.keys(query)
      .map((key: string) => {
        const k = key as keyof IGenomeQuery;
        return `where.${key}=${query[k]}`;
      })
      .join('&');
  }

  /**
   * Transform signalFilter into part of URL
   *
   * @private
   * @param {ISignalFilter} signalFilter
   * @returns {string}
   * @memberof Genome
   */
  private filterToUrl(signalFilter: ISignalFilter): string {
    let url = `kind=${signalFilter.kind}`;
    if (signalFilter.function) {
      url += `&function=${signalFilter.function}`;
    }
    if (signalFilter.ranks) {
      url += `&where.ranks=${signalFilter.ranks}`;
    }
    return url;
  }
}
