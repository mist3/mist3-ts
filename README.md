<img src="https://gitlab.com/mist3/mist3-ts/raw/master/assets/mist3-ts.logo.png" width=200px/>

![npm](https://img.shields.io/npm/v/mist3-ts.svg)
![License: CC0-1.0](https://img.shields.io/badge/License-CC0%201.0-blue.svg)
[![pipeline status](https://gitlab.com/mist3/mist3-ts/badges/master/pipeline.svg)](https://gitlab.com/mist3/mist3-ts/commits/master)
[![coverage report](https://gitlab.com/mist3/mist3-ts/badges/master/coverage.svg)](https://mist3.gitlab.io/mist3-ts/coverage)

# mist3-ts

> Under construction package, it shouldn't be really published yet.

## Install

```bash
npm install mist3-ts
```

## Usage

### Class: Genomes

#### fetchDetails()

```typescript
const version = 'GCF_000005845.2'
const genome = new Genome(version)
const details = genome.details()
```

#### fetchAllGenes()

```typescript
const version = 'GCF_000005845.2'
const genome = new Genome(version)
const allGenes = genome.fetchAllGenes()
```

#### fetchChemotaxis()

```typescript
const version = 'GCF_000005845.2'
const genome = new Genome(version)
const chemotaxis = genome.fetchChemotaxis()
```

### Class: Genes

#### fetchByIds()

```typescript
const genes = new Genes(version)
const geneIds = '29096360,29096359'
const allGenes = genes.fetchByIds(geneIds)
```

#### fetchByAnyField()

```typescript
const genes = new Genes(version)
const locus = 'VC0395_RS19590'
const allGenes = genes.fetchByAnyField(locus)
```

### Class: Taxonomy

#### getParents()

```typescript
const taxonomy = new Taxonomy()
const taxid = 362
const parents = taxonomy.getParents(taxid)
```

### Class: Aseq

```typescript
const aseq = new Aseq(version)
const aseqId = '6C4cMS5Ty-EVszF-Jh-nHA'
const seqInfo = aseq.fetch(aseqId)
```

### Class: GenomeStream

Experimental stream wrap around `Genome` class.

```javascript
import { GenomeStream } from 'mist3-ts';
import through2 from 'through2';

const sink = through2.obj((chunk, enc, next) => {
  next();
});
const version = 'GCF_002996345.1';
const gStream = new GenomeStream(100, logLevel)
const genes = []
const startPage = 11

gStream.fetchAllGenes(version, startPage).then(s => {
  s.on('data', gene => {
    // do something with genes
  })
  s.on('end', () => {
    // All done
  })
  s.pipe(sink)
})
```

## Alternative URL

MiST3-TS accepts the address of an alternative MiST3 API URL

Example:

```typescript
const version = 'GCF_000005845.2'
const localApi = 'http://localhost:5000/v1'
const genome = new Genome(version, localApi)
const details = genome.details()
```

## Documentation

[Developer's documentation](https://mist3.gitlab.io/mist3-ts/)

...to be continued.

Written with ❤ in Typescript.