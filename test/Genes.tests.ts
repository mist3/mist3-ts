/* tslint:disable comma-spacing */
/* tslint:disable quote-props */
/* tslint:disable key-spacing */
/* tslint:disable object-literal-sort-keys */
/* tslint:disable no-magic-numbers */
import * as chai from 'chai';

import { Genes } from '../src/Genes';

const expect = chai.expect;
const logLevel = 'silent'

describe('Genes', () => {
  const genes = new Genes(logLevel);
  describe('fetchByStableId()', () => {
    it('should give the details about the gene', async function() {
      this.timeout(60000);
      const expectedDetails = {
        id: 196959894,
        stable_id: 'GCF_000016245.1-VC0395_RS19590',
        component_id: 6050095,
        dseq_id: 'oF989tuACL20MiovRFbnRg',
        aseq_id: '6C4cMS5Ty-EVszF-Jh-nHA',
        accession: 'WP_000769922',
        version: 'WP_000769922.1',
        locus: 'VC0395_RS19590',
        old_locus: 'VC0395_A2871',
        location: 'complement(3021362..3022987)',
        strand: '-',
        start: 3021362,
        stop: 3022987,
        length: 1626,
        names: null,
        pseudo: false,
        notes: null,
        product: 'methyl-accepting chemotaxis protein',
        codon_start: 1,
        translation_table: 11,
        qualifiers: {},
        cds_location: 'complement(3021362..3022987)',
        cds_qualifiers: {
          inference: 'COORDINATES: similar to AA sequence:RefSeq:NP_230103.1',
        },
        ComponentId: 6050095,
        AseqId: '6C4cMS5Ty-EVszF-Jh-nHA',
        DseqId: 'oF989tuACL20MiovRFbnRg',
      }
      const stableId = 'GCF_000016245.1-VC0395_RS19590';
      const geneInfo = await genes.fetchByStableId(stableId);
      expect(geneInfo).eql(expectedDetails);
    });
  });
  describe('fetchByIds()', () => {
    it('should give the details about the gene', async function() {
      this.timeout(60000);
      const expectedDetails = [
        {
          id: 196959894,
          stable_id: 'GCF_000016245.1-VC0395_RS19590',
          component_id: 6050095,
          dseq_id: 'oF989tuACL20MiovRFbnRg',
          aseq_id: '6C4cMS5Ty-EVszF-Jh-nHA',
          accession: 'WP_000769922',
          version: 'WP_000769922.1',
          locus: 'VC0395_RS19590',
          old_locus: 'VC0395_A2871',
          location: 'complement(3021362..3022987)',
          strand: '-',
          start: 3021362,
          stop: 3022987,
          length: 1626,
          names: null,
          pseudo: false,
          notes: null,
          product: 'methyl-accepting chemotaxis protein',
          codon_start: 1,
          translation_table: 11,
          qualifiers: {},
          cds_location: 'complement(3021362..3022987)',
          cds_qualifiers: {
            inference: 'COORDINATES: similar to AA sequence:RefSeq:NP_230103.1',
          },
          ComponentId: 6050095,
          AseqId: '6C4cMS5Ty-EVszF-Jh-nHA',
          DseqId: 'oF989tuACL20MiovRFbnRg',
        },
      ];
      const id = '196959894';
      const geneInfo = await genes.fetchByIds(id);
      expect(geneInfo).eql(expectedDetails);
    });
    it('should give details of more than 30 genes', async function() {
      this.timeout(60000);
      const expectedResponseLength = 44
      const ids = [
        337919270, 337918818, 337918117, 394811660,
        394811399, 394811772, 394813538, 394813425,
        394813163, 394814281, 394814022, 394814393,
        394815844, 394815732, 394815472, 394816793,
        394816681, 394816422, 394817974, 394817715,
        394817603, 394819817, 394819704, 394819449,
        394820994, 394820564, 394820491, 394821725,
        394821612, 394821354, 394822680, 394822567,
        394822309, 394823590, 394823448, 394827012,
        394826892, 394826840, 394832592, 394832483,
        394832363, 394843785, 394843527, 394843414
      ].join(',')
      const geneInfo = await genes.fetchByIds(ids);
      expect(geneInfo.length).eql(expectedResponseLength);
    })
    it('should give return empty array if id is not found', async function() {
      this.timeout(60000)
      const expectedResponseLength = 0
      const ids = [
        737919270,
      ].join(',')
      const geneInfo = await genes.fetchByIds(ids);
      expect(geneInfo.length).eql(expectedResponseLength);
    })
    it('should return empty array if no id is passed', async function() {
      this.timeout(60000)
      const expectedResponseLength = 0
      const ids = ''
      const geneInfo = await genes.fetchByIds(ids);
      expect(geneInfo.length).eql(expectedResponseLength);
    })
  });
  describe('fetchByAnyField()', () => {
    it('should give the details about the gene', async function() {
      this.timeout(60000);
      const expectedDetails = [
        {
          id: 196959894,
          stable_id: 'GCF_000016245.1-VC0395_RS19590',
          component_id: 6050095,
          dseq_id: 'oF989tuACL20MiovRFbnRg',
          aseq_id: '6C4cMS5Ty-EVszF-Jh-nHA',
          accession: 'WP_000769922',
          version: 'WP_000769922.1',
          locus: 'VC0395_RS19590',
          old_locus: 'VC0395_A2871',
          location: 'complement(3021362..3022987)',
          strand: '-',
          start: 3021362,
          stop: 3022987,
          length: 1626,
          names: null,
          pseudo: false,
          notes: null,
          product: 'methyl-accepting chemotaxis protein',
          codon_start: 1,
          translation_table: 11,
          qualifiers: {},
          cds_location: 'complement(3021362..3022987)',
          cds_qualifiers: {
            inference: 'COORDINATES: similar to AA sequence:RefSeq:NP_230103.1',
          },
          ComponentId: 6050095,
          AseqId: '6C4cMS5Ty-EVszF-Jh-nHA',
          DseqId: 'oF989tuACL20MiovRFbnRg',
        },
      ];
      const locus = 'VC0395_RS19590';
      const geneInfo = await genes.fetchByAnyField(locus);
      expect(geneInfo).eql(expectedDetails);
    });
  });
  describe('getGeneHood', function() {
    this.timeout(10000);
    it('should pass', async () => {
			const stableId = 'GCF_000302455.1-A994_RS00140'
			const upstream = 2
      const downstream = 4
      const expectedListOfLoci = [
        "A994_RS00120",
        "A994_RS00125",
        "A994_RS00130",
        "A994_RS00135",
        "A994_RS00140",
        "A994_RS00145",
        "A994_RS00150"
      ]
      const items = await genes.getGeneHood(stableId, upstream, downstream);
      const listOfLoci = items.map((i: {locus: string}) => i.locus)
      expect(expectedListOfLoci).eql(listOfLoci);
		})
		it('should pass with any stable ID', async () => {
			const stableId = 'GCF_000006765.1-PA1105'
			const upstream = 2
      const downstream = 4
      const expectedListOfLoci = [
        'PA1101', 'PA1102',
        'PA1103', 'PA1104',
        'PA1105', 'PA1106',
        'PA1107'
      ]
      const items = await genes.getGeneHood(stableId, upstream, downstream);
      const listOfLoci = items.map((i: {locus: string}) => i.locus)
      expect(expectedListOfLoci).eql(listOfLoci);
    })
    it('should pass with any stable ID', async () => {
			const stableId = 'GCF_000006765.1-PA1105'
			const upstream = 17
      const downstream = 16
      const expectedListOfLoci = [
        "PA1089", "PA1090", 
        "PA1091", "PA1092",
        "PA1093", "PA1094",
        "PA1095", "PA1096",
        "PA1097", "PA1098",
        "PA1099", "PA1100",
        'PA1101', 'PA1102',
        'PA1103', 'PA1104',
        'PA1105', 'PA1106',
        'PA1107', 'PA1108',
        'PA1109', 'PA1110',
        "PA1111", "PA1112",
        "PA1112a", "PA1112.1", 
        "PA1112b", "PA1113", 
        "PA1114", "PA1115",
        "PA1116", "PA1117",
        "PA1118", "PA1119"
      ]
      const items = await genes.getGeneHood(stableId, upstream, downstream);
      const listOfLoci = items.map((i: {locus: string}) => i.locus)
      expect(expectedListOfLoci).eql(listOfLoci);
    })
    it('should pass with any stable ID in small components', async () => {
			const stableId = 'GCF_000264355.1-UUC_RS17710'
			const upstream = 17
      const downstream = 16
      const expectedListOfLoci = [
        "UUC_RS17700",
        "UUC_RS17705",
        "UUC_RS17710",
        "UUC_RS17715",
        "UUC_RS17720",
      ]
      const items = await genes.getGeneHood(stableId, upstream, downstream);
      const listOfLoci = items.map((i: {locus: string}) => i.locus)
      expect(listOfLoci).eql(expectedListOfLoci);
    })
  })
  describe.skip('using alternative API', () => {
    describe('fetchByIds', () => {
      it('should give details of more than 30 genes with alternative API', async function() {
        this.timeout(60000);
        const expectedResponseLength = 0
        const api = 'http://localhost:5000/v1'
        const alterGenes = new Genes(logLevel, api)
        const ids = [
          337919270, 337918818, 337918117, 394811660,
          394811399, 394811772, 394813538, 394813425,
          394813163, 394814281, 394814022, 394814393,
          394815844, 394815732, 394815472, 394816793,
          394816681, 394816422, 394817974, 394817715,
          394817603, 394819817, 394819704, 394819449,
          394820994, 394820564, 394820491, 394821725,
          394821612, 394821354, 394822680, 394822567,
          394822309, 394823590, 394823448, 394827012,
          394826892, 394826840, 394832592, 394832483,
          394832363, 394843785, 394843527, 394843414
        ].join(',')
        const geneInfo = await alterGenes.fetchByIds(ids);
        expect(geneInfo.length).eql(expectedResponseLength);
      })
    })
    describe('fetchByStableIds', () => {
      it('should give details of more than 30 genes with alternative API', async function() {
        this.timeout(60000);
        const expectedResponseLength = 10
        const api = 'http://localhost:5000/v1'
        const alterGenes = new Genes(logLevel, api)
        const stableIds = 'GCF_000006745.1-VC_RS06755,GCF_000006745.1-VC_RS06760,GCF_000006745.1-VC_RS06780,GCF_000006745.1-VC_RS09915,GCF_000006745.1-VC_RS09930,GCF_000006745.1-VC_RS18450,GCF_000006745.1-VC_RS18455,GCF_000006745.1-VC_RS18460,GCF_000006745.1-VC_RS18470,GCF_000006745.1-VC_RS18485'
        const geneInfo = await alterGenes.fetchByStableIds(stableIds);
        expect(geneInfo.length).eql(expectedResponseLength);
      })
    })
  })
});
