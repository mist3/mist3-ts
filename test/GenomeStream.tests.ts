/* tslint:disable comma-spacing */
/* tslint:disable quote-props */
/* tslint:disable key-spacing */
/* tslint:disable object-literal-sort-keys */
/* tslint:disable no-magic-numbers */
import chai from 'chai'
import through2 from 'through2'
import { GenomeStream } from '../src/GenomeStream';

const expect = chai.expect
const logLevel = 'silent'

describe('GenomesStream', () => {
  const version = 'GCF_002996345.1';
  describe('fetchAllGenes', () => {
    it('should work', function(done) {
      this.timeout(720000)
      this.retries(3)
      const sink = through2.obj((chunk, enc, next) => {
        next();
      });
      const gStream = new GenomeStream(100, logLevel)
      const genesPassed = []
      const numbersOfExpectedGenes = 1293

      gStream.fetchAllGenes(version).then(s => {
        s.on('data', data => genesPassed.push(data))
        s.on('end', () => {
          expect(genesPassed.length).eql(numbersOfExpectedGenes)
          done()
        })
        s.pipe(sink)
      })
    })
    it('should work starting from specific page', function(done) {
      this.timeout(720000)
      this.retries(3)
      const sink = through2.obj((chunk, enc, next) => {
        next();
      });
      const gStream = new GenomeStream(100, logLevel)
      const genesPassed = []
      const numbersOfExpectedGenes = 293
      const startPage = 11

      gStream.fetchAllGenes(version, startPage).then(s => {
        s.on('data', data => genesPassed.push(data))
        s.on('end', () => {
          expect(genesPassed.length).eql(numbersOfExpectedGenes)
          done()
        })
        s.pipe(sink)
      })
    })
  })
})
