// tslint:disable: object-literal-sort-keys
// tslint:disable: only-arrow-functions
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';

chai.use(chaiAsPromised)

const expect = chai.expect
const should = chai.should()
const logLevel = 'SILENT'

import { Taxonomy } from '../src/Taxonomy';

describe('Taxonomy', function() {
	const taxonomy = new Taxonomy(logLevel)
  describe('getParents', function() {
		it('should work with one', function() {
      this.timeout(60000)
			const taxid = 562
			const expected = [
				{
					id: 131567,
					parent_taxonomy_id: 1,
					name: 'cellular organisms',
					rank: 'no rank'
				},
				{
					id: 2,
					parent_taxonomy_id: 131567,
					name: 'Bacteria',
					rank: 'superkingdom'
				},
				{
					id: 1224,
					parent_taxonomy_id: 2,
					name: 'Proteobacteria',
					rank: 'phylum'
				},
				{
					id: 1236,
					parent_taxonomy_id: 1224,
					name: 'Gammaproteobacteria',
					rank: 'class'
				},
				{
					id: 91347,
					parent_taxonomy_id: 1236,
					name: 'Enterobacterales',
					rank: 'order'
				},
				{
					id: 543,
					parent_taxonomy_id: 91347,
					name: 'Enterobacteriaceae',
					rank: 'family'
				},
				{
					id: 561,
					parent_taxonomy_id: 543,
					name: 'Escherichia',
					rank: 'genus'
				},
				{
					id: 562,
					parent_taxonomy_id: 561,
					name: 'Escherichia coli',
					rank: 'species'
				}
			]
			return taxonomy.getParents(taxid).then((results) => {
				return expect(results).eql(expected)
			})
		})
		it('should return empty with invalid taxid', function() {
			this.timeout(60000)
			const taxid = 11676
			return taxonomy.getParents(taxid).then((result) => {
        return expect(result).to.be.empty
      })
		})
	})
})