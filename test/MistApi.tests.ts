import * as chai from 'chai';

import { MistApi } from '../src/MistApi';

const expect = chai.expect;

const LogLevel = 'silent'

const validVersion = "GCF_000302455.1"

describe('MistApi', () => {
  describe('use alternative base url', () => {
    it('should give the genome base url', () => {
      const alternativeUrl = 'https://localhost:5000/v1'
      const mistApi = new MistApi(LogLevel, alternativeUrl);
      const genomeBaseUrl = mistApi.getBaseUrl('genome');
      const expectedUrl = 'https://localhost:5000/v1/genomes';
      expect(genomeBaseUrl).eql(expectedUrl);
    });
    it('should use default base url if passed undefined', () => {
      const alternativeUrl = undefined;
      const mistApi = new MistApi(LogLevel, alternativeUrl);
      const genomeBaseUrl = mistApi.getBaseUrl('genome');
      const expectedUrl = 'https://api.mistdb.caltech.edu/v1/genomes';
      expect(genomeBaseUrl).eql(expectedUrl);
    });
  });
  describe('getBaseUrl', () => {
    it('should give the genome base url', () => {
      const mistApi = new MistApi(LogLevel);
      const genomeBaseUrl = mistApi.getBaseUrl('genome');
      const expectedUrl = 'https://api.mistdb.caltech.edu/v1/genomes';
      expect(genomeBaseUrl).eql(expectedUrl);
    });
  });
  describe('fetch (GET) for genomes', () => {
    const entity = 'genomes'
    it('should allow change the perPage', async function() {
      this.timeout(10000)
      const genesPerPage = 3
      const mistApi = new MistApi(LogLevel);
      const genesUrl = `${mistApi.getBaseUrl(entity)}/${validVersion}/genes?`;
      const genes = await mistApi.fetch(genesUrl, false, true, 1, genesPerPage)
      expect(genes.length).eql(genesPerPage);
    });
  });
});
