/* tslint:disable comma-spacing */
/* tslint:disable quote-props */
/* tslint:disable key-spacing */
/* tslint:disable object-literal-sort-keys */
/* tslint:disable no-magic-numbers */
import * as chai from 'chai';
import chaiAsPromised from 'chai-as-promised';

import { Aseq } from '../src/Aseq';

chai.use(chaiAsPromised)

const expect = chai.expect
const logLevel = 'silent'

describe('Aseq', () => {
  const aseq = new Aseq(logLevel);
  describe('fetch()', () => {
    it('should give the details about the gene', async function() {
      this.timeout(60000);
      const expectedDetails = {
        "pfam31": [
          {
            "name": "MCPsignal",
            "score": 145.1,
            "bias": 24.5,
            "c_evalue": 2.9e-46,
            "i_evalue": 1.6e-42,
            "hmm_from": 5,
            "hmm_to": 169,
            "hmm_cov": "..",
            "ali_from": 325,
            "ali_to": 503,
            "ali_cov": "..",
            "env_from": 321,
            "env_to": 506,
            "env_cov": "..",
            "acc": 0.98
          },
          {
            "name": "sCache_2",
            "score": 120,
            "bias": 0.2,
            "c_evalue": 1.4e-38,
            "i_evalue": 7.6e-35,
            "hmm_from": 4,
            "hmm_to": 153,
            "hmm_cov": "..",
            "ali_from": 36,
            "ali_to": 185,
            "ali_cov": "..",
            "env_from": 33,
            "env_to": 187,
            "env_cov": "..",
            "acc": 0.97
          },
          {
            "name": "dCache_2",
            "score": 75.4,
            "bias": 0.1,
            "c_evalue": 7e-25,
            "i_evalue": 3.9e-21,
            "hmm_from": 47,
            "hmm_to": 188,
            "hmm_cov": "..",
            "ali_from": 38,
            "ali_to": 180,
            "ali_cov": "..",
            "env_from": 28,
            "env_to": 190,
            "env_cov": "..",
            "acc": 0.93
          }
        ],
        "agfam2": [],
        "ecf1": [],
        "stp": {
          "ranks": [
            "chemotaxis",
            "mcp",
            "40H"
          ],
          "counts": {
            "dCache_2": 1,
            "MCPsignal": 1
          },
          "inputs": [
            "dCache_2"
          ],
          "cheHits": [
            {
              "name": "mcp-40H",
              "score": 338.9,
              "bias": 70.1,
              "c_evalue": 2.7e-104,
              "i_evalue": 2.2e-103,
              "hmm_from": 22,
              "hmm_to": 274,
              "hmm_cov": "..",
              "ali_from": 280,
              "ali_to": 532,
              "ali_cov": "..",
              "env_from": 259,
              "env_to": 539,
              "env_cov": "..",
              "acc": 0.96
            },
            {
              "name": "mcp-44H",
              "score": 183.7,
              "bias": 46.1,
              "c_evalue": 2.2e-57,
              "i_evalue": 1.8e-56,
              "hmm_from": 41,
              "hmm_to": 256,
              "hmm_cov": "..",
              "ali_from": 285,
              "ali_to": 500,
              "ali_cov": "..",
              "env_from": 283,
              "env_to": 505,
              "env_cov": "..",
              "acc": 0.98
            },
            {
              "name": "mcp-24H",
              "score": 158.1,
              "bias": 30.5,
              "c_evalue": 1.5e-49,
              "i_evalue": 1.2e-48,
              "hmm_from": 3,
              "hmm_to": 149,
              "hmm_cov": "..",
              "ali_from": 314,
              "ali_to": 463,
              "ali_cov": "..",
              "env_from": 305,
              "env_to": 470,
              "env_cov": "..",
              "acc": 0.9
            },
            {
              "name": "mcp-34H",
              "score": 109.9,
              "bias": 4.4,
              "c_evalue": 5e-35,
              "i_evalue": 4.1e-34,
              "hmm_from": 88,
              "hmm_to": 149,
              "hmm_cov": "..",
              "ali_from": 367,
              "ali_to": 428,
              "ali_cov": "..",
              "env_from": 365,
              "env_to": 431,
              "env_cov": "..",
              "acc": 0.97
            },
            {
              "name": "mcp-28H",
              "score": 108.3,
              "bias": 13.4,
              "c_evalue": 2.4e-34,
              "i_evalue": 2e-33,
              "hmm_from": 8,
              "hmm_to": 127,
              "hmm_cov": "..",
              "ali_from": 308,
              "ali_to": 427,
              "ali_cov": "..",
              "env_from": 304,
              "env_to": 433,
              "env_cov": "..",
              "acc": 0.92
            }
          ],
          "outputs": [],
          "version": 1
        },
        "id": "6C4cMS5Ty-EVszF-Jh-nHA",
        "length": 541,
        "sequence": "MKLKTQAYLLSAIILAALLALTATGLWTLRVASNLDNKARVTELFNSAYSILTEVEKLAQEGKMSEPEAKALATRLMRNNLYKDNEYVYVADENMTFVATPLDPQLHDTSFHDFKDGKGNSVGRLIQDVLRHQSGKLVEYTWTQKQADGSIEEKLSIARKTPHWGWVVGTGIGFNEVNERFWSTAQWQLSLCVVIAVAILSLLLVAIRKILLIIGGEPNEVRSAVQAVAQGRIRREFVIKAPKESIYGAVQQMNSSLADLVAKLEQSMVALRSELAGAGTRAKSIAELTDSQQQSTAMIATAMTEMASSANQVADSARDTAFNTDQADQQSQHTQKLIHNTVSNIQGLATQLQTASTAVADLDLDVKNIAKVLDVIGDIAEQTNLLALNAAIEAARAGEQGRGFAVVADEVRNLAGRTQTSTKEIQQMIHNLQEGSRNAIQTIQICGQTSQSSVQESENAASALALIVSALESVSSMSHQIATAAAEQTQVSDDIARRINMIEESGSKLSRVVMESHNSTQTLTKLARELEQWAAHFEVTR",
        "segs": [
          [
            7,
            29
          ],
          [
            193,
            207
          ],
          [
            295,
            310
          ],
          [
            384,
            405
          ],
          [
            460,
            476
          ]
        ],
        "coils": [],
        "tmhmm2": {
          "tms": [
            [
              7,
              29
            ]
          ],
          "topology": [
            [
              "i",
              1,
              6
            ],
            [
              "M",
              7,
              29
            ],
            [
              "O",
              30,
              541
            ]
          ]
        }
      };
      const aseqId = '6C4cMS5Ty-EVszF-Jh-nHA';
      const seqInfo = await aseq.fetch(aseqId);
      expect(seqInfo).eql(expectedDetails);
    });
  });
  describe('fetchMany()', () => {
    it('should give the details about the gene', async function() {
      this.timeout(60000);
      const aseqIdList = ['6C4cMS5Ty-EVszF-Jh-nHA', '4bhBp3qJd88fOXitc4eTYA']
      const seqInfoList = await aseq.fetchMany(aseqIdList);
      expect(seqInfoList.length).eql(2);
    }),
    it('it should throw with invalid aseq', async function() {
      this.timeout(60000);
      const aseqIdList = ['6C4cMS5Ty-EVszF-Jh-nHA', '4bhBp3qJd88fOXitc4eTYA', 'null']
      expect(aseq.fetchMany(aseqIdList)).to.be.rejected
    })
  })
});