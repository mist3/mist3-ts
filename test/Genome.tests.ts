/* tslint:disable comma-spacing */
/* tslint:disable quote-props */
/* tslint:disable key-spacing */
/* tslint:disable object-literal-sort-keys */
/* tslint:disable no-magic-numbers */
import chai from 'chai'
import { Genome } from '../src/Genome';

const expect = chai.expect
const logLevel = 'silent'

describe('Genomes', () => {
  const version = 'GCF_002996345.1';
  const versions = [
    'GCF_002996345.1',
    'GCF_000005845.2'
  ]
  const longVersionList = [
    'GCF_000170935.2', 'GCF_000222835.1', 'GCF_000165595.2', 'GCF_000741005.1',
    'GCF_000962775.1', 'GCF_001900865.1', 'GCF_000172275.2', 'GCF_000172595.4',
    'GCF_000239475.1', 'GCF_000300045.1', 'GCF_000501055.1', 'GCF_000501135.1',
    'GCF_000501015.1', 'GCF_000501175.1', 'GCF_000501595.1', 'GCF_000501615.1',
    'GCF_000501235.1', 'GCF_000501195.1', 'GCF_000501815.1', 'GCF_000691545.1',
    'GCF_000259345.1', 'GCF_000714705.1', 'GCF_000501915.1', 'GCF_000825665.1',
    'GCF_000568715.1', 'GCF_000523535.1', 'GCF_000019705.1', 'GCF_000568695.2',
    'GCF_000807295.1', 'GCF_001767415.1', 'GCF_002741765.1', 'GCF_002741665.1',
    'GCF_002741785.1', 'GCF_002741805.1', 'GCF_002741845.1', 'GCF_002741825.1',
    'GCF_000464455.1', 'GCF_000468115.1', 'GCF_000217655.1', 'GCF_000170955.2',
    'GCF_001887175.1', 'GCF_000465015.1', 'GCF_000465055.1', 'GCF_000465095.1',
    'GCF_000465035.1', 'GCF_000465075.1', 'GCF_000181875.2', 'GCF_000196215.1',
    'GCF_000260795.1', 'GCF_002916695.1', 'GCF_000619925.1', 'GCF_001749745.1',
    'GCF_000775995.1', 'GCF_001936295.1', 'GCF_000171775.1', 'GCF_000008685.2',
    'GCF_000012085.2', 'GCF_001660005.1', 'GCF_000568755.1', 'GCF_001936255.1',
    'GCF_000008185.1', 'GCF_000513775.1', 'GCF_000604125.1', 'GCF_000255555.1',
    'GCF_000421345.1', 'GCF_000195275.1', 'GCF_000304735.1', 'GCF_001922545.1',
    'GCF_900099615.1', 'GCF_000019685.1', 'GCF_000500065.1', 'GCF_000500045.1',
    'GCF_000242595.2', 'GCF_000373545.1', 'GCF_000445425.4', 'GCF_000413055.1',
    'GCF_000413015.1', 'GCF_000143985.1', 'GCF_000413035.1', 'GCF_000739475.1',
    'GCF_000222305.1', 'GCF_000412995.1', 'GCF_000212415.1', 'GCF_000214375.1',
    'GCF_000181895.2', 'GCF_000297095.1', 'GCF_000214355.1', 'GCF_900156105.1',
    'GCF_000219725.1', 'GCF_000755145.1', 'GCF_900167025.1', 'GCF_900167145.1',
    'GCF_000447675.1', 'GCF_000426705.1', 'GCF_000507245.1', 'GCF_000758165.1',
    'GCF_001945665.1', 'GCF_000184345.1', 'GCF_000166635.1', 'GCF_000021405.1',
    'GCF_002996345.1', 'GCF_000005845.2'
  ]
  const genome = new Genome(logLevel);
  describe('fetchDetails()', () => {
    it('should give the details about the genome', async function() {
      this.timeout(60000);
      const expectedDetails = {
        id: 6408,
        worker_id: null,
        accession: 'GCF_002996345',
        version: 'GCF_002996345.1',
        version_number: 1,
        genbank_accession: 'GCA_002996345',
        genbank_version: 'GCA_002996345.1',
        taxonomy_id: 139,
        name: 'Borreliella burgdorferi',
        refseq_category: 'na',
        bioproject: 'PRJNA224116',
        biosample: 'SAMN08049199',
        wgs_master: 'PHQB00000000.1',
        isolate: null,
        version_status: 'latest',
        assembly_level: 'Contig',
        release_type: 'Major',
        release_date: '2018-03-14',
        assembly_name: 'ASM299634v1',
        submitter: 'Public Health Agency of Canada',
        ftp_path: 'ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/002/996/345/GCF_002996345.1_ASM299634v1',
        superkingdom: 'Bacteria',
        phylum: 'Spirochaetes',
        class: 'Spirochaetia',
        order: 'Spirochaetales',
        family: 'Borreliaceae',
        genus: 'Borreliella',
        species: 'Borreliella burgdorferi',
        strain: null,
        stats: {},
        createdAt: '2018-09-02T02:59:24.639Z',
        updatedAt: '2020-03-26T02:30:06.772Z',
        WorkerId: null,
      };
      const details = await genome.fetchDetails(version);
      expect(details).eql(expectedDetails);
    });
  });
  describe('fetchDetailsMany()', () => {
    it('should give the details about the genome', async function() {
      this.timeout(60000);
      const expectedDetails = [
        {
          name: 'Borreliella burgdorferi',
        },
        {
          name: 'Escherichia coli str. K-12 substr. MG1655',
        }
      ]
      const details = await genome.fetchDetailsMany(versions);
      expect(details.length).eql(expectedDetails.length)
      expectedDetails.forEach((detail) => {
        const found = details.find((g: { name: string; }) => g.name === detail.name)
        // tslint:disable-next-line: no-unused-expression
        expect(found).to.exist
        // tslint:disable-next-line: no-unused-expression
        expect(found).to.not.be.empty
      })
    });
    it('should give the details about more than 100 genomes at once.', async function() {
      this.timeout(60000);
      const expectedDetails = [
        {
          name: 'Borreliella burgdorferi',
        },
        {
          name: 'Escherichia coli str. K-12 substr. MG1655',
        }
      ]
      const expectedNumberOfResults = longVersionList.length
      const details = await genome.fetchDetailsMany(longVersionList);
      expect(details.length).eql(expectedNumberOfResults)
      expectedDetails.forEach((detail) => {
        const found = details.find((g: { name: string; }) => g.name === detail.name)
        // tslint:disable-next-line: no-unused-expression
        expect(found).to.exist
        // tslint:disable-next-line: no-unused-expression
        expect(found).to.not.be.empty
      })
    });
  });
  describe('fetchAllGenes()', () => {
    it('should return an array with all genes from the genome', async function() {
      this.timeout(720000);
      const details = await genome.fetchAllGenes(version);
      expect(details.length).eql(1293);
    });
    it('should return an array with genes from page 11 and on from the genome', async function() {
      this.timeout(720000);
      const startPage = 11
      const details = await genome.fetchAllGenes(version, startPage);
      expect(details.length).eql(293);
    });
  });
  describe('fetchChemotaxis()', () => {
    it('should have the right number of items', async function() {
      this.timeout(360000);
      const details = await genome.fetchChemotaxis(version);
      expect(details.length).eql(16);
    })
    it('a component should have all the data', async function() {
      this.timeout(360000);
      const details = await genome.fetchChemotaxis(version);
      const expectedProperties = [
        'data',
        "id",
        "gene_id",
        "component_id",
        "signal_domains_version",
        "ranks",
        "counts",
        "inputs",
        "outputs",
        "ComponentId",
        "GeneId",
        "Component"
      ]
      expectedProperties.forEach(expected => expect(details[0].hasOwnProperty(expected), `Property "${expected}" not found.`).to.be.true)
    });
  });
  describe('search()', () => {
    it('should return an array with genomes related to search term', async function() {
      this.timeout(360000);
      const details = await genome.search("Methylomicrobium");
      expect(details.length).to.be.above(3);
    });
  });
  describe('find()', () => {
    it('should return an array with genomes matching query', async function() {
      this.timeout(360000);
      const query = {
        order: 'Vibrionales',
        assembly_level: 'Complete Genome'
      }
      const details = await genome.find(query);
      const numberOfCompleteGenomes = details.reduce((p: number, c: { assembly_level: string; }) => p += c.assembly_level === "Complete Genome" ? 1 : 0, 0)
      expect(details.length).eql(numberOfCompleteGenomes)
      expect(details.length).greaterThan(100);

    });
  });
  describe('fetchSignalGenes()', () => {
    it('should return an array with ecf genes from the genome', async function() {
      this.timeout(360000);
      const filter = {
        kind: 'ecf'
      }
      const details = await genome.fetchSignalGenes(version, filter);
      expect(details.length).eql(1);
    });
    it('should return an array with all signal genes from the genome if nothing is passed', async function() {
      this.timeout(360000);
      const details = await genome.fetchSignalGenes(version);
      expect(details.length).eql(32);
    });
    it('should be accept if passed function with ranks', async function() {
      this.timeout(360000);
      const filter = {
        kind: 'chemotaxis',
        ranks: 'chea'
      }
      const details = await genome.fetchSignalGenes(version, filter)
      expect(details.length).eql(2)
    });
    it('should accept function as second argument', async function() {
      this.timeout(360000);
      const filter = {
        kind: 'output',
        function: 'enzymatic'
      }
      const details = await genome.fetchSignalGenes(version, filter);
      expect(details.length).eql(3);
    });
  });
});
